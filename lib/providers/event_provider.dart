import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:wmad_cw2/model/event_model.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class EventProvider extends ChangeNotifier {
  final DatabaseReference _databaseReference =
      FirebaseDatabase.instance.ref().child('events');

  List<Event> _events = [];
  List<Event> get events => _events;

  Future<void> addEvent(Event event) async {
    try {
      final newEventRef = await _databaseReference.push();

      await newEventRef.set({
        'id': newEventRef.key,
        'eventname': event.eventName,
        'duration': event.duration,
        'seatcount': event.seatCount,
        'ticketprice': event.ticketPrice,
        'contactname': event.contactName,
        'contactnumber': event.contactNumber,
        'datetime': event.dateTime,
        'location': event.location,
        'description': event.description,
        'imageUrl': event.imageUrl
      });

      await fetchEvents();
    } catch (error) {
      print("Error adding event: $error");
    }
  }

  Future<void> updateEvent(Event event) async {
    try {
      print(event.id);
      final updateEventRef = _databaseReference.child(event.id!);

      Map<String, dynamic> updateFields = {
        'eventname': event.eventName,
        'duration': event.duration,
        'seatcount': event.seatCount,
        'ticketprice': event.ticketPrice,
        'contactname': event.contactName,
        'contactnumber': event.contactNumber,
        'datetime': event.dateTime,
        'location': event.location,
        'description': event.description,
      };

      if (event.imageUrl != null) {
        updateFields['imageUrl'] = event.imageUrl;
      }

      await updateEventRef.update(updateFields);

      await fetchEvents();
    } catch (error) {
      print("Error adding event: $error");
    }
  }

  Future<String?> uploadImage(File imageFile, String imageName) async {
    try {
      Reference imageRef = FirebaseStorage.instance
          .ref()
          .child("event_images")
          .child("$imageName.jpg");

      UploadTask uploadTask = imageRef.putFile(imageFile);
      TaskSnapshot snapshot = await uploadTask;

      String downloadURL = await snapshot.ref.getDownloadURL();
      return downloadURL;
    } catch (error) {
      print("Error uploading image: $error");
      return null;
    }
  }

  Future<void> fetchEvents() async {
    try {
      DatabaseEvent databaseEvent = await _databaseReference.once();
      DataSnapshot dataSnapshot = databaseEvent.snapshot;

      if (dataSnapshot.value != null) {
        Map<dynamic, dynamic>? eventDataMap =
            dataSnapshot.value as Map<dynamic, dynamic>?;

        if (eventDataMap != null) {
          _events.clear();
          eventDataMap.forEach((key, value) {
            Event event = Event(
              id: value['id'],
              eventName: value['eventname'],
              duration: value['duration'],
              seatCount: value['seatcount'],
              ticketPrice: value['ticketprice'],
              contactName: value['contactname'],
              contactNumber: value['contactnumber'],
              dateTime: value['datetime'],
              location: value['location'],
              description: value['description'],
              imageUrl: value['imageUrl'],
              videoUrl: value['videoUrl'],
            );
            _events.add(event);
          });

          notifyListeners();
        }
      }
    } catch (error) {
      print("Error fetching events: $error");
    }
  }

  Future<void> deleteEvent(String id) async {
    try {
      await _databaseReference.child(id).remove();
      await fetchEvents();
    } catch (error) {
      print("Error deleting event: $error");
      throw error;
    }
  }

  Future<List<Map<String, dynamic>>> searchEventsByName(
      String eventName) async {
    try {
      List<Map<String, dynamic>> searchResults = [];

      DatabaseEvent databaseEvent = await _databaseReference.once();
      DataSnapshot dataSnapshot = databaseEvent.snapshot;

      if (dataSnapshot.value != null) {
        Map<dynamic, dynamic>? eventDataMap =
            dataSnapshot.value as Map<dynamic, dynamic>?;

        if (eventDataMap != null) {
          eventDataMap.forEach((key, value) {
            Event event = Event(
              id: value['id'],
              eventName: value['eventname'],
              duration: value['duration'],
              seatCount: value['seatcount'],
              ticketPrice: value['ticketprice'],
              contactName: value['contactname'],
              contactNumber: value['contactnumber'],
              dateTime: value['datetime'],
              location: value['location'],
              description: value['description'],
              imageUrl: value['imageUrl'],
              videoUrl: value['videoUrl'],
            );

            if (event.eventName
                .toLowerCase()
                .contains(eventName.toLowerCase())) {
              searchResults.add({
                'id': event.id,
                'name': event.eventName,
                'contactnumber': event.contactNumber,
                'imageUrl': event.imageUrl,
                'duration': event.duration,
                'seatCount': event.seatCount,
                'ticketPrice': event.ticketPrice,
                'contactName': event.contactName,
                'dateTime': event.dateTime,
                'location': event.location,
                'description': event.description,
                'videoUrl': event.videoUrl,
              });
            }
          });
        }
      }

      return searchResults;
    } catch (error) {
      print("Error searching events by name: $error");
      throw error;
    }
  }
}
