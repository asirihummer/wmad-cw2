import 'package:flutter/cupertino.dart';
import 'dart:convert';
import "package:http/http.dart" as http;
import 'package:wmad_cw2/model/article_model.dart';


class ArticlesProvider with ChangeNotifier {
  ArticlesModel? _newsModel;
  String _selectedCategory = 'music';

  ArticlesModel? get news => _newsModel;

  String get selectedCategory => _selectedCategory;

  Future<void> fetchNewsByCategory(String category) async {
    try {
      _selectedCategory = category;
      final response = await http.get(Uri.parse(
          'https://newsapi.org/v2/everything?q=$category&apiKey=c0b58584422c486b866ab34605c1a745'));

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = json.decode(response.body);
        _newsModel = ArticlesModel.fromJson(data);
        notifyListeners();
      } else {
        throw Exception('Failed to load news data for category: $category');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }
}
