class Event {
  String? id;
  final String eventName;
  final String duration;
  final String seatCount;
  final String ticketPrice;
  final String contactName;
  final String contactNumber;
  final String dateTime;
  final String location;
  final String description;
  String? imageUrl;
  String? videoUrl;

  Event({
    this.id,
    required this.eventName,
    required this.duration,
    required this.seatCount,
    required this.ticketPrice,
    required this.contactName,
    required this.contactNumber,
    required this.dateTime,
    required this.location,
    required this.description,
    this.imageUrl,
    this.videoUrl,
  });
}