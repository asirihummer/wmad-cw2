import 'package:flutter/material.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [Colors.blueAccent,Colors.white],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
            )
        ),
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            Icon(
              Icons.event_outlined,
              size: 80,
              color: Colors.white,
            ),
            SizedBox(height: 20),
            Text('Event Elite',style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.white,
              fontSize: 32,
            ),),
            Text("Version 1.0.0",style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.white,
              fontSize: 18,
            ),)
          ],
        ),
      ),
    );
  }
}
