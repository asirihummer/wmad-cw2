import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wmad_cw2/model/event_model.dart';
import 'package:wmad_cw2/views/viewEvent.dart';
import '../providers/event_provider.dart';
import 'package:connectivity/connectivity.dart';

class AddEventsPage extends StatefulWidget {
  @override
  State<AddEventsPage> createState() => _AddEventsPageState();
}

class CustomSearchTextField extends StatelessWidget {
  final String hintText;
  final ValueChanged<String> onChanged;

  const CustomSearchTextField({
    Key? key,
    required this.hintText,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.black87),
      ),
      style: TextStyle(color: Colors.white),
      onChanged: onChanged,
    );
  }
}

class _AddEventsPageState extends State<AddEventsPage> {
  final EventProvider _eventProvider = EventProvider();
  final ImagePicker _picker = ImagePicker();
  File? _selectedImage;

  Future<void> _getImage() async {
    final pickedFile = await _picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _selectedImage = File(pickedFile.path);
      }
    });
  }

  List<Map<String, dynamic>> _dataSet = [];
  bool _isLoading = false;

  void _refreshDataSet() async {
    setState(() {
      _isLoading = true;
    });

    // Check if online
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      // If offline, load from cache
      await _loadCachedData();
    } else {
      // If online, fetch from firebase
      await _eventProvider.fetchEvents();
      // Save the fetched data to the cache
    }

    List<Event> events = _eventProvider.events;
    List<Map<String, dynamic>> eventsData = events
        .map((event) => {
      'id': event.id,
      'name': event.eventName,
      'contactnumber': event.contactNumber,
      'imageUrl': event.imageUrl,
      'duration': event.duration,
      'seatCount': event.seatCount,
      'ticketPrice': event.ticketPrice,
      'contactName': event.contactName,
      'dateTime': event.dateTime,
      'location': event.location,
      'description': event.description,
      'videoUrl': event.videoUrl,
    })
        .toList();
    if (connectivityResult != ConnectivityResult.none) {
      _saveDataToCache(eventsData);
    }

    setState(() {
      _dataSet = events.map((event) {
        return {
          'id': event.id,
          'name': event.eventName,
          'contactnumber': event.contactNumber,
          'imageUrl': event.imageUrl,
          'duration': event.duration,
          'seatCount': event.seatCount,
          'ticketPrice': event.ticketPrice,
          'contactName': event.contactName,
          'dateTime': event.dateTime,
          'location': event.location,
          'description': event.description,
          'videoUrl': event.videoUrl,
        };
      }).toList();
      _loadCachedData();
      _isLoading = false;
    });
  }

  Future<void> _saveDataToCache(List<Map<String, dynamic>> events) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String dataToCache = jsonEncode(events);
      await prefs.setString('events', dataToCache);
      print('saved');
      String? cachedData = prefs.getString('events');
      print(cachedData);
    } catch (e) {
      print("Error saving data to cache: $e");
    }
  }

  Future<void> _loadCachedData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? cachedData = prefs.getString('events');
    print(cachedData);
    var connectivityResult = await Connectivity().checkConnectivity();
    if (cachedData != null && connectivityResult == ConnectivityResult.none) {
      setState(() {
        _dataSet = List<Map<String, dynamic>>.from(jsonDecode(cachedData));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _refreshDataSet();
  }

  final TextEditingController _eventNameController = TextEditingController();
  final TextEditingController _durationController = TextEditingController();
  final TextEditingController _seatCountController = TextEditingController();
  final TextEditingController _ticketPriceController = TextEditingController();
  final TextEditingController _contactNameController = TextEditingController();
  final TextEditingController _contactNumberController =
  TextEditingController();
  final TextEditingController _dateTimeController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  Future<void> _searchEventByName(String searchstring) async {
    try {
      List<Map<String, dynamic>> searchResults =
      await _eventProvider.searchEventsByName(searchstring);

      setState(() {
        _dataSet = searchResults;
        _isLoading = false;
      });
    } catch (error) {
      print("Error searching events by name: $error");
    }
  }

  Future<void> _addEvents() async {
    try {
      if (_selectedImage != null) {
        String? imageUrl = await _eventProvider.uploadImage(
          _selectedImage!,
          _eventNameController.text,
        );

        if (imageUrl != null) {
          final newEvent = Event(
            eventName: _eventNameController.text,
            duration: _durationController.text,
            seatCount: _seatCountController.text,
            ticketPrice: _ticketPriceController.text,
            contactName: _contactNameController.text,
            contactNumber: _contactNumberController.text,
            dateTime: _dateTimeController.text,
            location: _locationController.text,
            description: _descriptionController.text,
            imageUrl: imageUrl,
          );

          await _eventProvider.addEvent(newEvent);

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Event Details Successfully Added")),
          );

          _eventNameController.clear();
          _durationController.clear();
          _seatCountController.clear();
          _ticketPriceController.clear();
          _contactNameController.clear();
          _contactNumberController.clear();
          _dateTimeController.clear();
          _locationController.clear();
          _descriptionController.clear();
          _selectedImage = null;

          _refreshDataSet();
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Failed to upload image 1")),
          );
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text("Please select an image")),
        );
      }
    } catch (error) {
      print("Error adding event: $error");
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Failed to add event 2")),
      );
    }
  }

  Future<void> _updateEvent(String id) async {
    try {
      String? imageUrl;

      if (_selectedImage != null) {
        imageUrl = await _eventProvider.uploadImage(
          _selectedImage!,
          _eventNameController.text,
        );
      }
      final newEvent = Event(
        id: id,
        eventName: _eventNameController.text,
        duration: _durationController.text,
        seatCount: _seatCountController.text,
        ticketPrice: _ticketPriceController.text,
        contactName: _contactNameController.text,
        contactNumber: _contactNumberController.text,
        dateTime: _dateTimeController.text,
        location: _locationController.text,
        description: _descriptionController.text,
        imageUrl: imageUrl,
      );

      await _eventProvider.updateEvent(newEvent);

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Event Details Successfully Updated")),
      );

      _eventNameController.clear();
      _durationController.clear();
      _seatCountController.clear();
      _ticketPriceController.clear();
      _contactNameController.clear();
      _contactNumberController.clear();
      _dateTimeController.clear();
      _locationController.clear();
      _descriptionController.clear();
      _selectedImage = null;

      _refreshDataSet();
    } catch (error) {
      print("Error Updating event: $error");
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Failed to update event")),
      );
    }
    _refreshDataSet();
  }

  Future<void> _deleteEvent(String id) async {
    try {
      await _eventProvider.deleteEvent(id);
      _refreshDataSet();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Event Successfully Deleted")),
      );
    } catch (error) {
      print("Error deleting event: $error");
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Failed to delete event")),
      );
    }
  }

  void showBottomSheet(String? id) async {
    if (id != null) {
      final prevEvents = _dataSet.firstWhere((element) => element['id'] == id);
      _eventNameController.text = prevEvents['name'];
      _durationController.text = prevEvents['duration'];
      _seatCountController.text = prevEvents['seatCount'];
      _ticketPriceController.text = prevEvents['ticketPrice'];
      _contactNameController.text = prevEvents['contactName'];
      _contactNumberController.text = prevEvents['contactnumber'];
      _dateTimeController.text = prevEvents['dateTime'];
      _locationController.text = prevEvents['location'];
      _descriptionController.text = prevEvents['description'];
    }

    showModalBottomSheet(
      context: context,
      elevation: 5,
      isScrollControlled: true,
      builder: (_) => Container(
        padding: EdgeInsets.only(
            top: 30,
            right: 15,
            bottom: MediaQuery.of(context).viewInsets.bottom + 5,
            left: 15),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                controller: _eventNameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Event Name",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _durationController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Duration",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _seatCountController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Seat Count",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _ticketPriceController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Ticket Price",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _contactNameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Contact Name",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _contactNumberController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Contact Number",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _dateTimeController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Start Date & Time",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
                onTap: () async {
                  DateTime? pickedDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime.now(),
                    lastDate: DateTime(2101),
                  );

                  if (pickedDate != null) {
                    TimeOfDay? pickedTime = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                    );

                    if (pickedTime != null) {
                      DateTime selectedDateTime = DateTime(
                        pickedDate.year,
                        pickedDate.month,
                        pickedDate.day,
                        pickedTime.hour,
                        pickedTime.minute,
                      );

                      _dateTimeController.text = selectedDateTime.toString();
                    }
                  }
                },
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _locationController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Location",
                  contentPadding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                ),
              ),
              const SizedBox(height: 15),
              TextField(
                controller: _descriptionController,
                maxLines: 3,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Description",
                ),
              ),
              const SizedBox(height: 15),
              GestureDetector(
                onTap: _getImage,
                child: CircleAvatar(
                  radius: 50,
                  backgroundColor: Colors.grey[200],
                  child: _selectedImage == null
                      ? Icon(Icons.add_a_photo,
                      size: 50, color: Colors.grey[800])
                      : ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.file(
                      _selectedImage!,
                      width: 100,
                      height: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 15),
              ElevatedButton(
                onPressed: () async {
                  if (id == null) {
                    await _addEvents();
                  }
                  if (id != null) {
                    await _updateEvent(id as String);
                  }

                  _eventNameController.text = "";
                  _durationController.text = "";
                  _seatCountController.text = "";
                  _ticketPriceController.text = "";
                  _contactNameController.text = "";
                  _contactNumberController.text = "";
                  _dateTimeController.text = "";
                  _locationController.text = "";
                  _descriptionController.text = "";
                  _selectedImage = null;

                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(id == null ? "Add New Event" : "Update"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFD1C4E9),
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("Events"),
      ),
      body: _isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg.jpeg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: CustomSearchTextField(
                hintText: 'Search Event',
                onChanged: (value) async {
                  print(value);
                  await _searchEventByName(value);
                },
              ),
            ),
            Expanded(
              child: ListView.builder(
                // prakash's part
                itemCount: _dataSet.length,
                itemBuilder: (context, index) => InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ViewEvent(
                          event: Event(
                            eventName: _dataSet[index]['name'],
                            contactNumber: _dataSet[index]
                            ['contactnumber'],
                            duration: _dataSet[index]['duration'],
                            seatCount: _dataSet[index]['seatCount'],
                            ticketPrice: _dataSet[index]['ticketPrice'],
                            contactName: _dataSet[index]['contactName'],
                            dateTime: _dataSet[index]['dateTime'],
                            location: _dataSet[index]['location'],
                            description: _dataSet[index]['description'],
                            imageUrl: _dataSet[index]['imageUrl'],
                            videoUrl: _dataSet[index]['videoUrl'],
                          ),
                          onRefresh: _refreshDataSet,
                        ),
                      ),
                    ).then((value) {
                      if (value == true) {
                        _refreshDataSet();
                      }
                    });
                  },
                  child: Card(
                    margin: EdgeInsets.all(15),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: ListTile(
                      contentPadding: EdgeInsets.all(15),
                      leading: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: _dataSet[index]['imageUrl'] != null
                            ? Image.network(
                          _dataSet[index]['imageUrl'],
                          width: 70,
                          height: 70,
                          fit: BoxFit.cover,
                        )
                            : Container(
                          width: 70,
                          height: 70,
                          color: Colors.blueGrey[600],
                          child: Icon(
                            Icons.event_note,
                            color: Colors.black87,
                            size: 40,
                          ),
                        ),
                      ),
                      title: Text(
                        _dataSet[index]['name'],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5),
                          Text(
                            'Ticket Price: ${_dataSet[index]['ticketPrice']}',
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Date & Time: ${_dataSet[index]['dateTime']}',
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'More info',
                            style: TextStyle(
                              color: Colors.blue,
                            ),
                          ),
                        ],
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () {
                              showBottomSheet(_dataSet[index]['id']);
                            },
                            icon: const Icon(Icons.edit),
                          ),
                          IconButton(
                            onPressed: () {
                              String? id = _dataSet[index]['id'];
                              if (id != null) {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text('Confirmation'),
                                      content: Text(
                                          'Are you sure you want to delete this event?'),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text('No'),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            _deleteEvent(id);
                                          },
                                          child: Text('Yes'),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                print("Event ID is null");
                              }
                            },
                            icon: const Icon(Icons.delete),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showBottomSheet(null),
        child: Icon(Icons.add),
      ),
    );
  }
}
