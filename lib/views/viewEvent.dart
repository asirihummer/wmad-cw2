import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:wmad_cw2/model/event_model.dart';

class ViewEvent extends StatefulWidget {
  final Event? event;
  final VoidCallback onRefresh;
  const ViewEvent({super.key, this.event, required this.onRefresh});
  @override
  State<ViewEvent> createState() => _ViewEventState();
}

class CustomEventDetailsWidget extends StatelessWidget {
  final String label;
  final String value;

  const CustomEventDetailsWidget({
    Key? key,
    required this.label,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            label,
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            value,
            style: const TextStyle(color: Colors.black),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class _ViewEventState extends State<ViewEvent> {
  bool isImageLoaded = false;
  late VideoPlayerController _videoPlayerController;
  ChewieController? _chewieController;

  @override
  void initState() {
    super.initState();
    if (widget.event != null && widget.event!.imageUrl != null) {
      isImageLoaded = true;
    }

    _videoPlayerController = VideoPlayerController.network(
      widget.event?.videoUrl ?? '',
    )..initialize().then((_) {
      setState(() {});
    });

    _videoPlayerController.addListener(() {
      if (_videoPlayerController.value.hasError) {
        print("Error: ${_videoPlayerController.value.errorDescription}");
      }
    });

    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
      looping: false,
      autoInitialize: true,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: const TextStyle(color: Colors.white),
          ),
        );
      },
    );

    _videoPlayerController.initialize().then((_) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  void _showVideo() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Chewie(
              controller: _chewieController!,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");
    final String formattedTicketPrice = widget.event != null
        ? _formatTicketPrice(widget.event?.ticketPrice ?? "", numberFormat)
        : "";
    final List<String> categories = [
      'Live',
      'Video',
    ];
    String selectedCategory = 'Live';
    if (widget.event == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFD1C4E9),
          title:
              const Text('Events Details View'),
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              icon: const Icon(Icons.arrow_back_ios)),
        ),
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg.jpeg'),
              fit: BoxFit.cover,
            ),
          ),
          child: SizedBox(
              child: Scrollbar(
            thumbVisibility: true,
            child: ListView.builder(
                itemCount: 1,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Text(widget.event?.eventName ?? "",
                            style: const TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            )),
                        const Padding(
                          padding: EdgeInsets.all(16.0),
                        ),
                        if (isImageLoaded)
                          Image.network(
                            widget.event?.imageUrl ??
                                "https://advisorretire.com/wp-content/plugins/pl-platform/engine/ui/images/default-landscape.png",
                          )
                        else
                          const CircularProgressIndicator(),
                        const Padding(
                          padding: EdgeInsets.all(4.0),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(4.0),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                Share.share(
                                    'Event Name : ${widget.event?.eventName ?? ""} , Event Date & Time : ${widget.event?.dateTime ?? ""} , Duration : ${widget.event?.duration ?? ""} , Seat Count : ${widget.event?.seatCount ?? ""} , Ticket Price : ${widget.event?.ticketPrice ?? ""} , Contact Name : ${widget.event?.contactName ?? ""} , Contact Number : ${widget.event?.contactNumber ?? ""} , Event Description : ${widget.event?.description ?? ""} , Image : ${widget.event?.imageUrl ?? ""} ',
                                    subject: 'Event Arena');
                              },
                              child: const Icon(Icons.share_outlined),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                Clipboard.setData(
                                  ClipboardData(
                                      text:
                                          'Event Name : ${widget.event?.eventName ?? ""} , Event Date & Time : ${widget.event?.dateTime ?? ""} , Duration : ${widget.event?.duration ?? ""} , Seat Count : ${widget.event?.seatCount ?? ""} , Ticket Price : ${widget.event?.ticketPrice ?? ""} , Contact Name : ${widget.event?.contactName ?? ""} , Contact Number : ${widget.event?.contactNumber ?? ""} , Event Description : ${widget.event?.description ?? ""} '),
                                );
                                // widget.event!.eventName!
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('Text copied to clipboard'),
                                  ),
                                );
                              },
                              child: const Icon(Icons.copy),
                            ),
                            ElevatedButton(
                              onPressed: () async {
                                final Uri uri = Uri.parse(
                                    'tel:${widget.event?.contactNumber ?? ""}');
                                await launchUrl(uri);
                              },
                              child: const Icon(Icons.call),
                            ),
                          ],
                        ),
                        const Padding(
                          padding: EdgeInsets.all(12.0),
                        ),
                        Text(
                          'Duration: ${widget.event?.duration ?? ""}',
                          style: const TextStyle(
                              color: Colors.black),
                        ),
                        Text(
                          "Seat Count : ${widget.event?.seatCount ?? ""}",
                          style: const TextStyle(color: Colors.black),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(12.0),
                        ),
                        CustomEventDetailsWidget(
                          label: "Ticket Price: ",
                          value: 'LKR $formattedTicketPrice',
                        ),
                        const Padding(
                          padding: EdgeInsets.all(12.0),
                        ),
                        CustomEventDetailsWidget(
                          label: "Event Date Time: ",
                          value: widget.event?.dateTime ?? "",
                        ),
                        const Padding(
                          padding: EdgeInsets.all(12.0),
                        ),
                        CustomEventDetailsWidget(
                          label: 'Description:',
                          value: widget.event?.description ?? "",
                        ),
                        const SizedBox(height: 20),
                        SizedBox(
                          height: 50,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: categories.length,
                            itemBuilder: (context, index) {
                              final category = categories[index];
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ElevatedButton(
                                  onPressed: () {
                                    selectedCategory = category;
                                    _showVideo();
                                  },
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        selectedCategory == category
                                            ? Colors.tealAccent
                                            : Colors.white,
                                  ),
                                  child: Text(category),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          )),
        ),
      );
    }
  }
}

String _formatTicketPrice(String? ticketPrice, NumberFormat numberFormat) {
  try {
    // Attempt to parse the ticket price as a double
    double parsedPrice = double.parse(ticketPrice ?? "0.0");
    return numberFormat.format(parsedPrice);
  } catch (e) {
    // Handle parsing errors, return an empty string as a fallback

    return "";
  }
}
