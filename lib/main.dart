import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wmad_cw2/providers/article_provider.dart';
import 'package:wmad_cw2/views/articleScreen.dart';
import 'package:wmad_cw2/views/splashScreen.dart';

import 'views/addEvents.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Platform.isAndroid
      ? await Firebase.initializeApp(
          options: const FirebaseOptions(
              apiKey: 'AIzaSyAFrQp6k8eCs4E9i3_z4-y8ntJIaxly0hU',
              appId: '1:143060293079:android:1f1c45e7d97cca9ef72792',
              messagingSenderId: '143060293079',
              projectId: 'mad-cw2-8dd29',
              storageBucket: 'mad-cw2-8dd29.appspot.com'
          ))
      : await Firebase.initializeApp();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ArticlesProvider()),
      ],
      child: const EntertainmentApp(),
    ),
  );
}

class EntertainmentApp extends StatelessWidget {
  const EntertainmentApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Events Main Arena',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: FutureBuilder(
        future: Future.delayed(const Duration(seconds: 4)),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return const EntertainmentAppHomePage(title: 'Events Main Arena');
          } else {
            return const SplashScreen();
          }
        },
      ),
    );
  }
}

class EntertainmentAppHomePage extends StatefulWidget {
  const EntertainmentAppHomePage({super.key, required this.title});

  final String title;

  @override
  State<EntertainmentAppHomePage> createState() =>
      _EntertainmentAppHomePageState();
}

class _EntertainmentAppHomePageState extends State<EntertainmentAppHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg.jpeg'),
            fit: BoxFit.cover,
          ),
        ),
        // child: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: 100,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddEventsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: const AssetImage('assets/images/backg.jpg'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(1),
                        BlendMode.darken,
                      ),
                      child: const Center(
                        child: Text(
                          'Events',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const ArticlePage()),
                  );
                },
                child: const Text(
                  'Events Articles',
                  style: TextStyle(fontSize: 28,color: Colors.black87),
                ),
              )
            ],
          ),
        ),
        // ),
      ),
    );
  }
}
